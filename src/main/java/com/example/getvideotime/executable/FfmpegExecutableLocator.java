package com.example.getvideotime.executable;

import it.sauronsoftware.jave.FFMPEGLocator;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

/**
 * ffmpeg软件路径获取
 * @author whx
 * @date 2022/2/8
 */
public class FfmpegExecutableLocator extends FFMPEGLocator {

    @Override
    protected String getFFMPEGExecutablePath() {
        // 返回你安装的ffmpeg路径
        return "/Library/software/ffmpeg5.0/ffmpeg";
    }
}
