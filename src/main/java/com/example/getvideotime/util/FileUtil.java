package com.example.getvideotime.util;

import java.io.File;
import java.util.*;

/**
 * 文件工具类
 * @author whx
 * @date 2022/2/8
 */
public class FileUtil {

    private static final File[] EMPTY_FILE_ARR = new File[0];
    /**
     * 获取文件夹下所有视频文件
     *
     * @param filePath
     * @param videoSuffix
     * @return
     */
    public static List<File> getVideoFiles(File filePath, List<String> videoSuffix) {
        if (filePath == null) {
            return new ArrayList<>();
        }
        List<File> allFile = new ArrayList<>();
        allFile.addAll(Arrays.asList(Optional.ofNullable(filePath.listFiles(file -> {
            if (!file.isDirectory()) {
                for (String suffix : videoSuffix) {
                    if (file.getPath().endsWith(suffix)) {
                        return true;
                    }
                }
            } else {
                allFile.addAll(getVideoFiles(file, videoSuffix));
            }
            return false;
        })).orElse(EMPTY_FILE_ARR)));
        return allFile;
    }
}
