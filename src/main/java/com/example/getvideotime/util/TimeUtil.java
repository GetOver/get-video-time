package com.example.getvideotime.util;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 * @author whx
 * @date 2022/2/8
 */
public class TimeUtil {


    public static String formatTime(long second){
        return formatTime(second,null);
    }

    public static String formatTime(long timestamp,String format){
        if(format == null){
            format = "HH:mm:ss";
        }
        try {
            Date date = new Date(timestamp);
            SimpleDateFormat df = new SimpleDateFormat(format);
            df.setTimeZone(TimeZone.getTimeZone("GMT+0:00"));
            return df.format(date);
        } catch (Exception e) {
            return "时间转换错误";
        }
    }
}
