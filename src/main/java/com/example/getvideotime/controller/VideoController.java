package com.example.getvideotime.controller;

import com.example.getvideotime.entity.R;
import com.example.getvideotime.service.VideoService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author whx
 * @date 2022/2/8
 */
@RestController
@RequestMapping("video")
@AllArgsConstructor
public class VideoController {

    private final VideoService videoService;

    /**
     * 获取文件夹下视频总时长
     * @param path 文件路径
     * @return
     */
    @GetMapping("getTime")
    public R getTime(String path,Integer studyHour){
        return videoService.getVideoTimeFromPath(path,studyHour);
    }
}
