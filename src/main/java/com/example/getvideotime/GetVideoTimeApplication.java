package com.example.getvideotime;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GetVideoTimeApplication {

    public static void main(String[] args) {
        SpringApplication.run(GetVideoTimeApplication.class, args);
    }

}
