package com.example.getvideotime.service.impl;

import com.example.getvideotime.entity.R;
import com.example.getvideotime.entity.VideoTime;
import com.example.getvideotime.service.VideoService;
import com.example.getvideotime.util.FileUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.File;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

import static com.example.getvideotime.util.TimeUtil.formatTime;
import static com.example.getvideotime.util.VideoUtil.readVideoTimeFromCommand;

/**
 * @author whx
 * @date 2022/2/8
 */
@Service
@Slf4j
public class VideoServiceImpl implements VideoService {

    @Value("${video.suffix}")
    private List<String> videoSuffix;

    @Value("${video.ffmpeg-path}")
    private String ffmpegPath;

    @Override
    public R getVideoTimeFromPath(String path,Integer studyHour) {
        if(studyHour == null){
            studyHour = 2;
        }
        File filePath = new File(path);
        if(filePath.exists()){
            List<File> files = FileUtil.getVideoFiles(filePath, videoSuffix);
            if(files.isEmpty()){
                return R.fail(String.format("%s 该路径下未找到视频文件，请检查路径是否有误或者是否在配置文件中配置视频格式",path));
            }
            TreeMap<String,String> details = new TreeMap<>();
            TreeMap<Integer,VideoTime> tasks = new TreeMap<>();
            TreeMap<String,String> taskDetails = new TreeMap<>();
            AtomicInteger index = new AtomicInteger(1);
            AtomicLong videoTime = new AtomicLong();
            long studyHourMilSecond = (long) studyHour * 3600 * 1000;
            long sum = 0;
            for (File file : files) {
                try {
                    long time = readVideoTimeFromCommand(file.getAbsolutePath(),ffmpegPath);
                    sum += time;
                    videoTime.addAndGet(time);
                    String detail = formatTime(time);
                    if(videoTime.longValue() < studyHourMilSecond){
                        taskDetails.put(file.getName(),detail);
                    }else{
                        tasks.put(index.intValue(),new VideoTime(formatTime(videoTime.longValue()),taskDetails));
                        videoTime.set(0);
                        index.incrementAndGet();
                        taskDetails = new TreeMap<>();
                        taskDetails.put(file.getName(),detail);
                    }
                    log.info(file.getName()+"："+detail);
                    details.put(file.getName(),detail);
                } catch (Exception e) {
                    log.error("获取视频时长失败：" + e.getMessage());
                    details.put(file.getName(),"获取失败");
                    e.printStackTrace();
                }
            }
            String sumStr = formatTime(sum);
            log.info("总时长：" + sumStr);
            return R.data(new VideoTime(sumStr,details,tasks));
        }
        return R.fail("%s 该路径不存在，请检查路径是否有误");
    }
}
