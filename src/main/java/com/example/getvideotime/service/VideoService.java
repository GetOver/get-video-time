package com.example.getvideotime.service;

import com.example.getvideotime.entity.R;

/**
 * @author whx
 * @date 2022/2/8
 */
public interface VideoService {

    R getVideoTimeFromPath(String path,Integer studyHour);

}
