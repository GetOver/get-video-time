package com.example.getvideotime.entity;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;

/**
 * @author whx
 * @date 2022/2/8
 */
@Data
@AllArgsConstructor
public class VideoTime {
    /**
     * 总时长
     */
    private String totalTime;
    /**
     * 时长明细 文件名-时长
     */
    private SortedMap<String,String> details;

    /**
     * 每日任务
     */
    private SortedMap<Integer, VideoTime> tasks;

    public VideoTime(String totalTime, SortedMap<String, String> details) {
        this.totalTime = totalTime;
        this.details = details;
    }
}
