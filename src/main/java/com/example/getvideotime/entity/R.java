package com.example.getvideotime.entity;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * 返回类
 * @author whx
 * @date 2022/2/8
 */
@Data
@AllArgsConstructor
public class R<T> {

    private int code;
    private String message;
    private T data;

    public static R success(){
        return new R(200,"操作成功",null);
    }

    public static R fail(String msg){
        return new R(500,msg,null);
    }

    public static <T>R data(T data){
        return new R(200,"操作成功",data);
    }
}
