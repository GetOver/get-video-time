# 方法1 直接安装 
## 1.1 下载ffmpeg
http://www.ffmpeg.org/download.html
![img_1.png](img_1.png)
![img.png](img.png)
## 1.2 双击可执行文件ffmpeg
![img_2.png](img_2.png)
## 1.3 查看版本
```
cd /Library/software/ffmpeg5.0
./ffmpeg -version
```
![img_3.png](img_3.png)
# 方法2 基于homebrew编译后安装
## 2.1 环境
以下安装实例基于mac M1 
且已安装homebrew

## 2.2 创建安装目录并进入该文件夹
```
cd /Library/software
```

## 2.3 下载源码
如果下载速度慢的可以直接用加速器到 [github](https://github.com/FFmpeg/FFmpeg/releases) 下载源码然后放到ffmpeg文件夹下

```
git clone https://git.ffmpeg.org/ffmpeg.git ffmpeg3.0-source
```

## 2.4 执行脚本，指定安装目录
```
./configure --prefix=/Library/software/ffmpeg3.0
```

## 2.5 编译
```
make
```

## 2.6 安装
```
make install
```

## 2.7 查看安装版本
```
/Library/software/ffmpeg5.0/bin/ffmpeg -version
```

