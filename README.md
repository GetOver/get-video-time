# GetVideoTime

#### 介绍
通过ffmpeg获取视频时长

#### 博客介绍
详细可参考[博客介绍](https://blog.csdn.net/qq_24950043/article/details/122830251)


#### 使用说明

1.  根据doc/ffmpeg安装.md 安装ffmpeg
2.  将配置文件中的ffmpeg路径设置为你自己的
3.  运行项目
4.  访问video/getTime?path=xx&studyHour=2接口
